<?php

use Duo\RDStation\RDStation;

Route::get('/rdstation', function () {
    dd(RDStation::account()->AccountInfo());
});

Route::get('/rdstation/callback', function () {
    abort_if(!RDStation::callbackProcess(request('code')), 400, 'Erro ao processar callback.');

    return 'Code definido com sucesso!';
});
