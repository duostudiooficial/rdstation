<?php namespace Duo\RDStation;


class RDStationEvents
{

    //Eventos Padrão ------------------------------------------------------

    public function conversion()
    {
        //TODO: POST https://api.rd.services/platform/conversions?api_key=[api-key-token]
        //TODO: POST https://api.rd.services/platform/events
    }

    public function opportunity()
    {
        //TODO: POST https://api.rd.services/platform/events
    }

    public function opportunity_win()
    {
        //TODO: POST https://api.rd.services/platform/events
    }

    public function opportunity_lost()
    {
        //TODO: POST https://api.rd.services/platform/events
    }

    // --------------------------------------------------------------------

    //Eventos de E-commerce -----------------------------------------------

    public function order_placed()
    {
        //TODO: POST https://api.rd.services/platform/events
    }

    public function order_placed_item()
    {
        //TODO: POST https://api.rd.services/platform/events
    }

    public function cart_abandoned()
    {
        //TODO: POST https://api.rd.services/platform/events
    }

    public function cart_abandoned_item()
    {
        //TODO: POST https://api.rd.services/platform/events
    }

    // --------------------------------------------------------------------

    //Eventos de Chat -----------------------------------------------------

    public function chat_started()
    {
        //TODO: POST https://api.rd.services/platform/events
    }

    public function chat_finished()
    {
        //TODO: POST https://api.rd.services/platform/events
    }

    // --------------------------------------------------------------------

    //Eventos de Chamada --------------------------------------------------

    public function call_finished()
    {
        //TODO: POST https://api.rd.services/platform/events
    }

    // --------------------------------------------------------------------

    //Eventos de Mídia ----------------------------------------------------

    public function media_playback_started()
    {
        //TODO: POST https://api.rd.services/platform/events
    }

    public function media_playback_stopped()
    {
        //TODO: POST https://api.rd.services/platform/events
    }

    // --------------------------------------------------------------------

    //Batch de Eventos ----------------------------------------------------

    public function batch()
    {
        //TODO: POST https://api.rd.services/platform/events/batch
    }

    // --------------------------------------------------------------------



}
