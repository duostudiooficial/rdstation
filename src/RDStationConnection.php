<?php

    namespace Duo\RDStation;

    use Carbon\Carbon;
    use GuzzleHttp\Client;
    use Illuminate\Support\Facades\App;
    use Illuminate\Support\Facades\Storage;

    class RDStationConnection
    {

        protected $base_uri      = 'https://api.rd.services';
        protected $url_callback  = null;
        protected $api_token     = null;
        protected $client_id     = null;
        protected $client_secret = null;
        protected $code          = null;
        protected $access_token  = null;
        protected $refresh_token = null;
        protected $expires_in    = null;

        /**
         * @return string
         */
        public function getBaseUri(): string
        {
            return $this->base_uri;
        }

        /**
         * @return null
         */
        public function getUrlCallback()
        {
            return $this->url_callback ?? config('rdstation.url_callback');
        }

        /**
         * @param null $url_callback
         */
        public function setUrlCallback($url_callback)
        {
            $this->url_callback = $url_callback;
        }

        /**
         * @return null
         */
        public function getApiToken()
        {
            return $this->api_token;
        }

        /**
         * @param null $api_token
         */
        public function setApiToken($api_token)
        {
            $this->api_token = $api_token;
        }

        /**
         * @return null
         */
        public function getClientId()
        {
            return $this->client_id ?? config('rdstation.client_id');
        }

        /**
         * @param null $client_id
         */
        public function setClientId($client_id)
        {
            $this->client_id = $client_id;
        }

        /**
         * @return null
         */
        public function getClientSecret()
        {
            return $this->client_secret ?? config('rdstation.client_secret');
        }

        /**
         * @param null $client_secret
         */
        public function setClientSecret($client_secret)
        {
            $this->client_secret = $client_secret;
        }


        /**
         * @return null
         */
        public function getCode()
        {
            return $this->code ?? config('rdstation.code');
        }

        /**
         * @param null $code
         */
        public function setCode($code)
        {
            $this->code = $code;
        }

        /**
         * @return null
         */
        public function getAccessToken()
        {
            return $this->access_token ?? config('rdstation.access_token');
        }

        /**
         * @param null $access_token
         */
        public function setAccessToken($access_token)
        {
            $this->access_token = $access_token;
        }

        /**
         * @return null
         */
        public function getRefreshToken()
        {
            return $this->refresh_token ?? config('rdstation.refresh_token');
        }

        /**
         * @param null $refresh_token
         */
        public function setRefreshToken($refresh_token)
        {
            $this->refresh_token = $refresh_token;
        }

        /**
         * @return null
         */
        public function getExpiresIn()
        {
            return $this->expires_in;
        }

        /**
         * @param null $expires_in
         */
        public function setExpiresIn($expires_in)
        {
            $this->expires_in = $expires_in;
        }


        // --------------------------------------------------------------------------------

        public static function run($method, $uri, $options = null)
        {

            $connection = new RDStationConnection();
            if (!$connection->getAccessToken() || Carbon::now() > config('rdstation.expires_in')) {
                $connection->refreshToken();
            }

            $client   = new Client(['base_uri' => $connection->getBaseUri()]);
            $response = $client->request($method, $uri, [
                'headers' => [
                    "content-type"  => "application/json",
                    "Authorization" => "Bearer ".$connection->getAccessToken()
                ],
                'json'    => $options
            ]);

            if ($response->getStatusCode() != 200) {
                //TODO: Tratar retorno de erros
            }

            return json_decode($response->getBody()->getContents());

        }

        // --------------------------------------------------------------------------------

        public function openAuthDiolog()
        {

            abort_if(!$this->getClientId(), 400, 'Client ID não foi definido.');
            abort_if(!$this->getUrlCallback(), 400, 'URL de Callback não foi definida.');

            return App::abort(301, '', ['Location' => $this->getBaseUri().'/auth/dialog?client_id='.$this->getClientId().'&redirect_url='.$this->getUrlCallback()]);

        }

        public function RefreshToken()
        {
            abort_if(!$this->getClientId(), 400, 'Client ID não foi definido.');
            abort_if(!$this->getClientSecret(), 400, 'Client Secret não foi cadastrado.');

            if (!$this->getCode()) {
                return $this->openAuthDiolog();
            }

            $client   = new Client(['base_uri' => $this->getBaseUri()]);
            $response = $client->request('POST', '/auth/token', [
                'query' => ($this->getRefreshToken()) ? [
                    'client_id'     => $this->getClientId(),
                    'client_secret' => $this->getClientSecret(),
                    'refresh_token' => $this->getRefreshToken()
                ] : [
                    'client_id'     => $this->getClientId(),
                    'client_secret' => $this->getClientSecret(),
                    'code'          => $this->getCode()
                ]
            ]);

            if ($response->getStatusCode() != 200) {
                //TODO: Tratar retorno de erros
            }

            $response = json_decode($response->getBody()->getContents());

            $this->setAccessToken($response->access_token);
            $this->setRefreshToken($response->refresh_token);
            $this->setExpiresIn(Carbon::now()->addSeconds($response->expires_in));

            $this->storeConfig();

            return $response;
        }

        public function RevokeToken()
        {
            //TODO: RevokeToken POST https://api.rd.services/auth/revoke
        }

        public function storeConfig()
        {
            Storage::put('rdstation/config.json', json_encode([
                'url_callback'  => $this->url_callback,
                'api_token'     => $this->api_token,
                'client_id'     => $this->client_id,
                'client_secret' => $this->client_secret,
                'code'          => $this->code,
                'access_token'  => $this->access_token,
                'refresh_token' => $this->refresh_token,
                'expires_in'    => $this->expires_in,
            ]));
        }


    }
