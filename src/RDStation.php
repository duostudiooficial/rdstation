<?php

    namespace Duo\RDStation;

    /**
     * Class RDStation
     * @package Duo\RDStation
     */
    class RDStation
    {

        public static function conversion()
        {

            //TODO: Atalho para conversão padrão.

        }

        /**
         * @return RDStationAccount
         */
        public static function account()
        {
            return (new RDStationAccount());
        }

        /**
         * @return RDStationContacts
         */
        public static function contacts()
        {
            return (new RDStationContacts());
        }

        /**
         * @return RDStationCustomFields
         */
        public static function custom_fields()
        {
            return (new RDStationCustomFields());
        }

        /**
         * @return RDStationEvents
         */
        public static function events()
        {
            return (new RDStationEvents());
        }

        /**
         * @return RDStationFunnels
         */
        public static function funnels()
        {
            return (new RDStationFunnels());
        }

        /**
         * @return RDStationWebhooks
         */
        public static function webhooks()
        {
            return (new RDStationWebhooks());
        }



        // ------------------------------------------------------------------------------------------

        /**
         * Retorna as variáveis de configuração do RDStation
         *
         * @param string|null $key
         * @param string|null $default
         *
         * @return array|string
         */
        public static function config($key = null, $default = null)
        {

            if (!(bool)defined('RD_CONFIG_PATH')) {
                RDStation::setConfigFile();
            }

            $config = (array) json_decode(file_get_contents( constant('RD_CONFIG_PATH') ));

            if (is_null($key)) {
                return $config;
            }

            return $config[$key] ?? $default;

        }

        /**
         * Processa o callback retornado pelo RDStation
         *
         * @param string $code
         *
         * @return boolean
         */
        public static function callbackProcess($code)
        {
            abort_if(!$code, 400, 'Code não foi definido.');

            $conn = new RDStationConnection();
            $conn->setCode($code);
            $conn->storeConfig();
            return true;
        }

        /**
         * Define a constante com a pasta onde será salvo o arquivo de configuração.
         *
         * @param string|null $path
         */
        public static function setConfigFile($path = null)
        {

            abort_if(( $path && substr($path, -5) != '.json' ), 400, 'Arquivo deve ser um arquivo JSON');

            $path = $path ?? 'rdstation/config.json';

            $configFile = (function_exists('storage_path')) ? storage_path('app/'.$path) : realpath('./').'/'.$path;

            if(!file_exists($configFile)) {
                mkdir(str_replace( last(explode('/', $configFile)) , '', $configFile), 0700);
                abort_if(!copy(__DIR__.'/../config/config.json', $configFile), 400, 'Problema para salvar o arquivo de config.json');
            }

            define('RD_CONFIG_PATH', $configFile);

        }

    }
