<?php

    use Duo\RDStation\RDStation;

    if (! function_exists('abort')) {
        function abort($code, $message = '')
        {
            throw new Exception($message, $code);
        }
    }

    if (! function_exists('abort_if')) {
        function abort_if($boolean, $code, $message = '')
        {
            if ($boolean) {
                abort($code, $message);
            }
        }
    }

    if (! function_exists('config')) {
        function config($key = null, $default = null)
        {
            return RDStation::config(str_replace('rdstation.', '', $key), $default);
        }
    }
