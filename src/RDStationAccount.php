<?php

    namespace Duo\RDStation;

    /**
     * Class RDStationAccount
     *
     * É possível obter algumas informações da conta do RD Station Marketing por meio dos respectivos endpoints.
     *
     * @package Duo\RDStation
     */
    class RDStationAccount
    {

        /**
         * Retorna o nome da sua conta do RD Station Marketing.
         *
         * @return array [
         *     'name' => 'Account Name'
         * ]
         */
        public function AccountInfo()
        {
            return RDStationConnection::run('GET', 'marketing/account_info');
        }

        // --------------------------------------------------------------------------------

        /**
         * Retorna o código de rastreamento do RD Station Marketing para que possa ser incorporado em sites ou CMS.
         *
         * @return array [
         *      'path' => 'https://d335luupugsy2.cloudfront.net/js/loader-scripts/8d2892c6-e22c-2c2d-b15a-36916776e5e7-loader.js'
         * ]
         */
        public function TrackingCode()
        {
            return RDStationConnection::run('GET', 'marketing/tracking_code');
        }


    }
