<?php

    namespace Duo\RDStation;

    use Illuminate\Support\ServiceProvider;

    class RDStationServiceProvider extends ServiceProvider
    {
        /**
         * Register services.
         *
         * @return void
         */
        public function register()
        {
            //
        }

        /**
         * Bootstrap services.
         *
         * @return void
         */
        public function boot()
        {
            
            $this->publishes([
                __DIR__.'/../config/rdstation.php' => config_path('rdstation.php')
            ], 'rdstation');

            $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
            
        }

    }
