<?php namespace Duo\RDStation;


class RDStationWebhooks
{

    public function list()
    {
        return RDStationConnection::run('GET', 'integrations/webhooks');
    }

    public function create()
    {
        //TODO: POST https://api.rd.services/integrations/webhooks
    }

    public function update()
    {
        //TODO: PUT https://api.rd.services/integrations/webhooks/{uuid}
    }

    public function delete()
    {
        //TODO: DELETE https://api.rd.services/integrations/webhooks/{uuid}
    }






}
