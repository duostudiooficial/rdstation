# RDStation API PHP

## Instalação no Laravel

1. Utilizando Composer, execute o comando abaixo:
```
$ composer require duostudio/rdstation
```

2. No arquivo ```config/app.php```, adicione o _ServiceProvider_ dentro da seção ```providers```:
```
Duo\RDStation\RDStationServiceProvider::class,
```

3. Execute o comando abaixo para copiar o arquivo de configurações para a pasta ```config```:
```
$ php artisan vendor:publish --provider="Duo\RDStation\RDStationServiceProvider"
```

4. Pra finalizar, insira as váriaveis de ambiente no seu arquivo ```.env``` e preencha-as de acordo com os dados da sua aplicação e os que a RD te fornece:
```
RD_URL_CALLBACK=http://localhost:8080/rdstation/callback
RD_CLIENT_ID="m3u-cl13nt-1d"
RD_CLIENT_SECRET="m3u-cl13nt-s3cr3t"
RD_CODE="m3u-c0d3"
```

5. Pronto! Para utilizar a biblioteca, use os comandos próprios de cada model (_RDStationConnection, RDStationAccount, RDStationCustomFields..._) ou execute qualquer requisição para a API do RD utilizando o exemplo abaixo:
```
use Duo\RDStation\RDStation;

RDStation::conversion([]);
```
#### Variáveis .env

### PHP puro utilizando Composer
```
$ composer require duostudio/rdstation
```
```json
{
    "require": {
        "duostudio/rdstation": ""
    }
}
```

```php
<?php
require 'vendor/autoload.php';

use Duo\RDStation\RDStation;

RDStation::conversion([]);
```
