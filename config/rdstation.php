<?php

use Duo\RDStation\RDStation;

return [
    'url_callback'  => env('RD_URL_CALLBACK', RDStation::config('url_callback')),
    'client_id'     => env('RD_CLIENT_ID', RDStation::config('client_id')),
    'client_secret' => env('RD_CLIENT_SECRET', RDStation::config('client_secret')),
    'code'          => env('RD_CODE', RDStation::config('code')),
] + RDStation::config();
